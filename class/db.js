var mysql = require('mysql');
var config = require('./config.json');
var pool = mysql.createPool(config.db);

var db = {
    selectJobs : function () {
        pool.query('SELECT * FROM `jobs`', function (err, rows, fields) {
            if (err) {
                console.log("DB ERROR: " + err);
            } else {
                console.log(rows);
            }
        });
    }
}

module.exports = db;