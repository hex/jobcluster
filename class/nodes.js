var nodes = {
	nodes : new Map(),
	add : function (socket, id, name) {
		this.nodes.set(id, { socket : socket, name : name });
		console.log(id + " -> new node start : " + name);
	},
	list : function () {
		var list = new Array();
		this.nodes.forEach(function(value, key, mapa) {
  			list.push("<b>" + value.name + "</b> - id: " + key);
		});
		return list;
	},
	drop : function (id) {
		this.nodes.forEach(function (value, key, map) {
			if (id === key) {
				console.log(id + " -> node down : " + value.name);
				nodes.nodes.delete(key);
			}
		});
	}
}

module.exports = nodes;
