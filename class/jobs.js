var md5 = require('md5');
var jobs = {
	jobs : new Map(),
    getData : function (id) {},
    upData : function (id) {},
	add : function (id, node, wclass) {
		this.jobs.set(id, { "node" : node, "wstatus" : 1, "wclass" : wclass });
		console.log(node + " --> job start : " + id);
	},
	list : function () {
		var list = new Array();
		this.jobs.forEach(function(value, key, mapa) {
  			list.push("<b>" + key + "</b> - node: " + value.node + " - wclass : " + value.wclass + " - status: " + value.wstatus );
		});
		return list;
	},
	drop : function (id) {
		this.jobs.forEach(function (value, key, map) {
			if (id === key) {
				console.log(value.node + " --> job start : " + id);
				jobs.jobs.delete(key);
			}
		});
	},
    mkId : function () {
        var timest = new Date().getTime();
        var random = timest + Math.random();
        return md5(random);
    }
}

module.exports = jobs;