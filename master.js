// SERVIDOR WEB
var config = require('./config.json');
var express = require('express');
var serveStatic = require('serve-static');
var sb = require('spellbook');
var _ = require('lodash');
var app = express();
var nodes = require('./class/nodes.js');
var jobs = require('./class/jobs.js');

app.use(serveStatic('web', {'index': ['index.html', 'index.htm']}))
app.listen(config.httpPort);

var io = require('socket.io')({
	transports: ['websocket'],
});

io.attach(config.masterPort);

console.log("[JOBCLUSTER] RUNNING MASTER... ");
if ( config.httpPort === 80) {
	console.log("Webservice: http://" + config.httpHost);
} else {
	console.log("Webservice: http://" + config.httpHost + ":" + config.httpPort);
}

io.on('connection', function(socket){
	socket.on('stats', function () {
	        socket.emit('stats', { jobs : jobs.list(), nodes : nodes.list() });
	});

	socket.on('start', function (data) {
		nodes.add(socket, socket.id, data.name);
	});

	socket.on('disconnect', function () {
		nodes.drop(socket.id);
	});

	socket.on('requestJob', function (data) {
		jobs.add(data.id, nodes.nodes.get(socket.id).name, data.wclass);
        socket.emit('job', { wclass : data.wclass, id : data.id });
	});

	socket.on('endJob', function (data) {
		jobs.drop(data.id);
	});
})
