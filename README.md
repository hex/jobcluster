## JOBCLUSTER

# Install:
```sh
npm install
```

# RUN MASTER:
```sh
node master.js
```

# RUN NODE:
```sh
node node.js
```