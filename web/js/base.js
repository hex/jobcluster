var socket = io.connect('http://localhost:4568');

setInterval(function () {
	socket.emit('stats');
}, 1000);

// EVENTS
socket.on('stats', function(data){
        $('#jobs').html(data.jobs.join('<br>'));
        $('#nodes').html(data.nodes.join('<br>'));
});

// CLASES
var server = {
	check : function () {
		//socket.emit('check', { server : socket.id });
	},
	list : function () {
	},
	start : function () {
	},
	stop : function () {
	},
	add : function () {
	},
	delete : function () {
	},
	help : function () {
	}
}

var job = {
	info : function () {
	},
	start : function () {
	},
	status : function () {
	},
	delete : function () {
	},
	stop : function () {
	},
	up : function () {
	},
	down : function () {
	}
}
