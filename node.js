var config = require('./config.json');
var server = config.masterHost + ":" + config.masterPort;
var socket = require('socket.io-client')("http://" + server);
var os = require("os");
var async = require('async');
var jobs = require('./class/jobs');
var sb = require('spellbook');

console.log("[JOBCLUSTER] RUNNING NODE...");

var works = [];
if (config.works.length > 0) {
	console.log("Carregant feines: " + config.works.length);
	config.works.forEach(function (work) {
		for (var i = 0; i < work.number; i++) {
			works.push({ wclass : work.wclass, wstatus : 0 });
		}
	});

	console.log("Feines: " + JSON.stringify(works));

	async.forever(
		function (done) {
			async.each(works, function (work, done2) {
				if (work.wstatus === 0) {
					socket.emit('requestJob', { "id" : jobs.mkId(), "wclass" : work.wclass });
				}
				done2();
			},
			function () {
				setTimeout(function () {
					done();
				}, 5000 );
			});
		},
		function (err) {
			console.log(err);
		}
	);

} else {
	console.log("No works configured!");
	process.exit(0);
}

socket.on('connect', function (data){
	console.log("-> Conexio: " + server);
	socket.emit("start", { name : os.hostname() });
});

socket.on('disconnect', function (data) {
	console.log("<- Desconexio: " + server);
});

socket.on('job', function (data) {
	var worker;
	console.log("-> Job recibed: " + data.wclass);
	for (var i = 0; i < works.length; i++) {
		if (works[i].wclass === data.wclass && works[i].wstatus == 0) {
			works[i].wstatus = 1;
			worker = i;
			break;
		}
	}

	setTimeout(function () {
		socket.emit('endJob', { id : data.id });
		console.log("feina acabada: " + data.wclass)
		works[worker].wstatus = 0;
	}, sb.random(5000, 80000) );
});